import React, {Component} from 'react';
import { connect } from 'react-redux';



class Panier extends Component {    
    constructor(props) {
        super(props);

        this.state= {
            commands:[]
        }
    }


    render() {
        const { commands } = this.props;
        console.log("panier.js",commands)
        return (
            <ul className="dropdown-menu">
                <li className="header">Vos commandes</li>
                    <li>
                        <ul className="menu">
                        { this.state.commands.map((command) => {                                                        
                            return(
                                <li>
                                    <a href="#">                            
                                    <h4>
                                        {command.nom}
                                        <small><i className="fa fa-money"></i> {command.prix}</small>
                                    </h4>
                                    <p>{command.type}</p>
                                    </a>
                                </li>
                            )
                        })}
                        </ul>
                    </li>
            </ul>
        );      
    }
}

const mapStateToProps = (state)=>{
    console.log("state",state)
    console.log("5555",state.addedItems)
    return {
      commands: state.addedItems
    };
    
}

export default connect(mapStateToProps, null)(Panier);

