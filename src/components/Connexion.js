import React, {Component} from 'react';

export default class Connexion extends Component {
    render() {
        return (
            <div class="login-box">
                <div class="login-logo">
                    <a href="../../index2.html"><b>Resto</b>Share</a>
                </div>
                <div class="login-box-body">
                    <form action="/bo/liste-resto" method="get">
                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" placeholder="Email"/>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="Password"/>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <button class="btn btn-success pull-right">Se connecter</button>
                    </form>
                </div>
             </div>            
        )
    }
}