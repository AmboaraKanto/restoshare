import React, { Component } from 'react';
import datas from '../datas';


var thumbnail = {
    position : 'relative'
}

var caption = {
    position: 'absolute',
    top: '80%',
    left: '0',
    width: '100%',
    color: 'white',
    size: '17px',
    marginLeft: '7%'
}

export default class FicheResto extends Component {
    componentWillMount() {
        const id = this.props.match.params.id;
        console.log("id", id);
        const allRestaurant = this.getRestaurants();
        console.log("allRestaurant", allRestaurant);
        //const restaurant = allRestaurant.find(item => item.id===id);
        for (var i = 0; i < allRestaurant.length; i++) {
            if (allRestaurant[i].id === id) {
                console.log("Atoooo")
                console.log("le seul ", allRestaurant[i])
                console.log("Atoooo2")
                this.state.restaurant = allRestaurant[i]
                    
            }
        }

        //this.setState({restaurant:restaurant});
        console.log("----datas ", this.state.restaurant);
    }

    getRestaurants = () => {
        return datas;

    }



    constructor(props) {
        super(props);
        this.state = {
            restaurant: []
        };
    };

    render() {
        return (
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#activity" data-toggle="tab">Images</a></li>                            
                    </ul>
                    
                    {this.state.restaurant.plat.map((resto) => {
                    return (
                        <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="post">
                            <div class="row margin-bottom">
                                <div class="col-sm-6">
                                    <img class="img-responsive" style={thumbnail} src="../img/photo1.png" alt="Photo"/>
                                    <div style={caption}>
                                        <p>{resto.plat} - {resto.prix}</p>
                                        <button onClick={() => this.props.addToCart(resto.plat)} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img class="img-responsive" style={thumbnail} src="../img/photo2.png" alt="Photo"/>
                                            <div style={caption}>
                                                <p>{resto.plat} - {resto.prix}</p>  
                                                <button onClick={() => this.props.addToCart(resto.plat)} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>                                                       
                                            </div>
                                            <br/>
                                            <img class="img-responsive" style={thumbnail} src="../img/photo3.jpg" alt="Photo"/>
                                            <div style={caption}>
                                                <p>{resto.plat} - {resto.prix}</p>
                                                <button onClick={() => this.props.addToCart(resto.plat)} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <img class="img-responsive" style={thumbnail} src="../img/photo4.jpg" alt="Photo"/>
                                                <div style={caption}>
                                                    <p>{resto.plat} - {resto.prix}</p>
                                                    <button onClick={() => this.props.addToCart(resto.plat)} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                                </div>
                                            <br/>
                                            <img class="img-responsive" style={thumbnail} src="../img/photo1.png" alt="Photo"/>
                                                <div style={caption}>
                                                    <p>{resto.plat} - {resto.prix}</p>
                                                    <button onClick={() => this.props.addToCart(resto.plat)} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                                    
                </div>
                    )
                })}
                </div>    
            </div>           
        )        
    }
}