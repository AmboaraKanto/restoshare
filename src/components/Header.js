import React, {Component} from 'react';
import Panier from './Panier';

export default class Header extends Component {
    
    render(){
        return (
            <header className="main-header">
                <a href="#" className="logo">
                    <span className="logo-lg"><b>RESTO</b>share</span>
                </a>
                <nav className="navbar navbar-static-top">
                    <a href="#" className="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span className="sr-only">Toggle navigation</span>
                    </a>
                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            <li className="dropdown messages-menu">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                                    <i className="fa fa-shopping-cart"></i>
                                </a>
                                <Panier />                                                                
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        )
    }
}