import React, {Component} from 'react';
import datas from '../datas.js';
import {Link} from 'react-router-dom'

var styles = {
    marginLeft : '20%'
};

export default class ListeRestoBO extends Component {
    componentDidMount() {
        this.getRestaurants();
        console.log("----datas ",this.state.words)
      }
    
      getRestaurants = () => {
       /* const wordRef = firebase.database().ref('words');
        let wordState = [];
        wordRef.on('value', (snapshot) => {      
          for (let word in wordRef) {
            wordState.push({
              id : "123",
              word : "567",
              meaning: "tqinalikq"
            })
          }
        }) */
    
        /*let wordState = [];
        wordState.push({
          id : "123",
          word : "567",
          meaning: "tqinalikq"
        })*/    
    
        this.setState({
          words : datas
        })
    
      }
    
      constructor(props) {
        super(props);
        this.state = {
            words: [],
            advanced: []
        };
      };



    render() {
        const {words} = this.state;        
        return (
                <div style={styles}>
                    <div className="box-body">
                        <h1>Liste des restaurants</h1>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nom</th>
                                    <th>Adresse</th>
                                    <th>Telephone</th>
                                    <th>Cuisinier</th>
                                </tr>
                            </thead>                    
                            <tbody>
                            { words.map((word) => {
                                return (
                                    <tr>
                                        <td><Link to={"/bo/fiche-resto/"+word.id}>{word.id}</Link></td>
                                        <td>{word.name}</td>
                                        <td>{word.address}</td>
                                        <td>{word.phone}</td>
                                        <td>{word.gerant["first"]} {word.gerant["last"]}</td>
                                    </tr>
                                )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
        );
    }
    
}