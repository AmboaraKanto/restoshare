import React, {Component} from 'react';

var styles = {
    marginLeft : '3%'
}

export default class Menu extends Component {
    render() {
        return (
            <aside class="main-sidebar" styles={styles}>
                <section class="sidebar">                    
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">                                                               
                            <i class="fa fa-user"></i> <span>Nom</span>
                            <br/>
                            <input type="text" name="q" class="form-control" class="Search..." />
                            <br/>                        
                            <i class="fa fa-map-marker"></i>
                            <span>Adresse</span>
                            <br/>
                            <input type="text" name="q" class="form-control" class="Search..." />
                            <br/>                   
                            <i class="fa fa-heart"></i>
                            <span>Spécialité</span>
                            <br/>
                            <input type="text" name="q" class="form-control" class="Search..." />    
                            <br/><br/>
                            <button class="btn btn-md btn-primary pull-right">Rechercher</button>
                        </div>
                    </form>
                </section>
            </aside> 
        )
    }
}