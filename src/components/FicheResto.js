import React, { Component } from 'react';
import datas from '../datas';

var styles = {
    marginLeft: '20%'
};


export default class FicheResto extends Component {
    componentWillMount() {
        const id = this.props.match.params.id;
        console.log("id", id);
        const allRestaurant = this.getRestaurants();
        console.log("allRestaurant", allRestaurant);
        //const restaurant = allRestaurant.find(item => item.id===id);
        for (var i = 0; i < allRestaurant.length; i++) {
            if (allRestaurant[i].id === id) {
                console.log("Atoooo")
                console.log("le seul ", allRestaurant[i])
                console.log("Atoooo2")
                this.state.restaurant = allRestaurant[i]
                    
            }
        }

        //this.setState({restaurant:restaurant});
        console.log("----datas ", this.state.restaurant);
    }

    getRestaurants = () => {
        return datas;

    }

    constructor(props) {
        super(props);
        this.state = {
            restaurant: []
        };
    };

    render() {
        return (
            <div style={styles}>
                <div className="box-body">
                    <h1>Restaurant restaurants</h1>
                    <table id="example1" class="table table-bordered table-striped">
                        {this.state.restaurant.nom}
                        <thead>
                            <tr>
                                <th>Plat</th>
                                <th>Nom</th>
                                <th>Prix</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.restaurant.plat.map((resto) => {
                                return (
                                    <tr>
                                        <td>{resto.plat}</td>
                                        <td>{resto.nom}</td>
                                        <td>{resto.prix}</td>
                                        <td>{resto.type}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}