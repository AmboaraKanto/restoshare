import React, {Component} from 'react';
import { addToCart } from './reducers/actions/cardAction';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'

var styles = {
    height : '980px'    
};

var thumbnail = {
    position : 'relative'
}

var caption = {
    position: 'absolute',
    top: '80%',
    left: '0',
    width: '100%',
    color: 'white',
    size: '17px',
    marginLeft: '7%'
}

const mapDispatchToProps = (dispatch) => {
    console.log("dispatch", dispatch)
    return {
      addToCart: (id) => dispatch(addToCart(id))
    }
  }

class Main extends Component {
    
    render() {
        return (
           <div class="content-wrapper">
                <section class="content-header">                     
                </section>
                <section class="content" style={styles}>
                    { this.props.words.map((word) => {
                    return (
                    <div class="row">
                        <div class="col-md-3">
                            {/* Profil resto */}
                            <div class="box box-primary">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="../img/user4-128x128.jpg" alt="User profile picture"/>
                                    
                                        
                                            <h1 class="text-center">
                                                {word.name}
                                            </h1>         
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                        <b>Cuisine</b> <a class="pull-right">Malgache</a>
                                        </li>
                                        <li class="list-group-item">
                                        <b>Chef cuisinier</b> <a class="pull-right">Chef {word.gerant["first"]} {word.gerant["last"]}</a>
                                        </li>
                                        <li class="list-group-item">
                                        <b>Contact</b> <a class="pull-right">{word.phone}</a>
                                        </li>
                                    </ul>

                                    <b><Link to={"/listePlats/"+word.id}>{word.id}</Link>Voir</b>
                                </div>
                            </div>
                            {/* A propos */}
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">A propos</h3>
                                </div>                                
                                <div class="box-body">
                                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                                    <p class="text-muted">{word.address}</p>
                                    <hr/>
                                    <strong><i class="fa fa-pencil margin-r-5"></i> Menus phares</strong>
                                    <p>
                                        <span class="label label-danger">Ravitoto sy henakisoa</span>
                                        <br/>
                                        <span class="label label-success">Anana sy saucisse</span>
                                        <br/>
                                        <span class="label label-info">Lelan'omby</span>
                                        <br/>
                                        <span class="label label-warning">Voanjobory sy henakisoa</span>
                                    </p>
                                    <hr/>
                                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                                    <p>{word.about}</p>
                                </div>                                
                            </div>
                        </div>

                        {/* le a droite de l'ecran */}
                        <div class="col-md-9">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#activity" data-toggle="tab">Images</a></li>                            
                                </ul>

                                {/* image */}
                                <div class="tab-content">
                                    <div class="active tab-pane" id="activity">
                                        <div class="post">
                                            <div class="row margin-bottom">
                                                <div class="col-sm-6">
                                                    <img class="img-responsive" style={thumbnail} src="../img/photo1.png" alt="Photo"/>
                                                    <div style={caption}>
                                                        <p>{word.plat[0].plat} - {word.plat[0].prix}</p>
                                                        <button onClick={() => this.props.addToCart(word.plat[0])} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <img class="img-responsive" style={thumbnail} src="../img/photo2.png" alt="Photo"/>
                                                            <div style={caption}>
                                                                <p>{word.plat[1].plat} - {word.plat[1].prix}</p>  
                                                                <button onClick={() => this.props.addToCart(word.plat[1])} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>                                                       
                                                            </div>
                                                            <br/>
                                                            <img class="img-responsive" style={thumbnail} src="../img/photo3.jpg" alt="Photo"/>
                                                            <div style={caption}>
                                                                <p>{word.plat[2].plat} - {word.plat[2].prix}</p>
                                                                <button onClick={() => this.props.addToCart(word.plat[2])} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <img class="img-responsive" style={thumbnail} src="../img/photo4.jpg" alt="Photo"/>
                                                                <div style={caption}>
                                                                    <p>{word.plat[3].plat} - {word.plat[3].prix}</p>
                                                                    <button onClick={() => this.props.addToCart(word.plat[3])} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                                                </div>
                                                            <br/>
                                                            <img class="img-responsive" style={thumbnail} src="../img/photo1.png" alt="Photo"/>
                                                                <div style={caption}>
                                                                    <p>{word.plat[4].plat} - {word.plat[4].prix}</p>
                                                                    <button onClick={() => this.props.addToCart(word.plat[4])} class="btn btn-sm btn-success"><span class="fa fa-plus"></span></button>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>    
                        </div>   
                    </div>
                    )
                })}                    
                </section>                
            </div>
        )
    }
}

export default connect(null, mapDispatchToProps)(Main);