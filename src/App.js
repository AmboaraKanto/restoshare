import React, { Component } from 'react';
import Header from './components/Header.js';
import Menu from './components/Menu.js';
import Main from './components/Main.js';


import firebase from './firebase.js';
import datas from './datas.js';
import ListeRestoBO from './components/ListeRestoBO.js';

class App extends Component {  
  
  componentDidMount() {
    this.getRestaurants();
    console.log("----datas ",this.state.words)
  }

  getRestaurants = () => {
   /* const wordRef = firebase.database().ref('words');
    let wordState = [];
    wordRef.on('value', (snapshot) => {      
      for (let word in wordRef) {
        wordState.push({
          id : "123",
          word : "567",
          meaning: "tqinalikq"
        })
      }
    }) */

    /*let wordState = [];
    wordState.push({
      id : "123",
      word : "567",
      meaning: "tqinalikq"
    })*/    

    this.setState({
      words : datas
    })

  }

  constructor(props) {
    super(props);
    this.state = {
        words: [],
        advanced: []
    };
  };

  render() {
    return (
      <div>
         <Header />
         <Menu />
         <Main words={this.state.words} />
      </div>
    );
  }
}

export default App;
