import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {withRouter, Route, Link, BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';
import Connexion from './components/Connexion';
import * as serviceWorker from './serviceWorker';
import ListeRestoBO from './components/ListeRestoBO';
import rootReducer from './components/reducers/cardReducer';
import FicheResto from './components/FicheResto';
import ListePlat from './components/ListePlat';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware } from 'redux';

const store = createStore(rootReducer,applyMiddleware(createLogger()));
console.log(store.getState());


ReactDOM.render(
    <Provider store={store}>
         <Router>
            <Route exact path="/" component={App} />
            <Route path="/listePlats/:id" component={ListePlat}/>
            <Route path="/connexion" component={Connexion} />
            <Route path="/bo/liste-resto" component={ListeRestoBO}/>
            <Route path="/bo/fiche-resto/:id" component={FicheResto}/>
        </Router>
    </Provider>,
    document.getElementById('root')
     
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
