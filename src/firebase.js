import firebase from 'firebase';

var config = {
  apiKey: "AIzaSyAtiAeoa6P7uzs0q9hQHiBiev5IAVETNl4",
  authDomain: "restoshare-e58a9.firebaseapp.com",
  databaseURL: "https://restoshare-e58a9.firebaseio.com",
  projectId: "restoshare-e58a9",
  storageBucket: "restoshare-e58a9.appspot.com",
  messagingSenderId: "482668893569"
};

firebase.initializeApp(config);
export default firebase;